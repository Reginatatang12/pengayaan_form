<?php
    include "config.php";

        if(isset($_POST['simpan'])){
            // mengambil data dari form
            $id_kelas=$_POST['id_kelas'];
            $nama_kelas=$_POST['nama_kelas'];
            $prodi=$_POST['prodi'];
            $fakultas=$_POST['fakultas'];
            
            // validasi
            $sql = "SELECT * FROM kelas WHERE id_kelas='$id_kelas'";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                ?>
                    <div class="alert alert-danger alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong> Data sudah ada</strong>
                    </div>
                <?php
            }else{
            //proses simpan
                $sql = "INSERT INTO kelas(id_kelas,nama_kelas, prodi, fakultas) VALUES ('$id_kelas','$nama_kelas','$prodi','$fakultas')";
                if ($conn->query($sql) === TRUE) {
                    header("Location:kelas.php");
                }
            }
            $conn->close();
        }
?>
<!-- akhir proses tambah data -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Menambahkan data kelas</title>
</head>

<body>
<h1>FORM PENAMBAHAN DATA KELAS</h1>
    <form action="tambah_kelas.php" method="POST">
        <div class="row justify-content-center">
            <div class="row">
                <div class="col-5 border border-primary mt-2 p-3">
                    <div class= "form-group">
                        <label for="id_Kelas" class="form-label">ID Kelas</label>
                        <input type="text" name="id_Kelas" maxlength="20" class="form-control" required>
                    </div>
                    <div class= "form-group">
                        <label for="nama_Kelas" class="form-label">Nama Kelas</label>
                        <input type="text" name="nama_kelas" maxlength="255" class="form-control" required>
                    </div>
                    <div class= "form-group">
                        <label for="prodi" class="form-label">Program Studi</label>
                        <input type="text" name="prodi" maxlength="255" class="form-control" required>
                    </div>
                    <div class= "form-group">
                        <label for="fakultas" class="form-label">Fakultas</label>
                        <input type="text" name="fakultas" maxlength="255" class="form-control" required>
                    </div>
                    <input class ="btn btn-primary" type="submit" name="simpan" value="Simpan">
                    <a class="btn btn-danger" href="kelas.php">Batal</a>
                </div>
            </div>
        </div>
    </form>
</body>
