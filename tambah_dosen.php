<?php
    include "config.php";

        if(isset($_POST['simpan'])){
            // mengambil data dari form
            $id_dosen=$_POST['id_dosen'];
            $foto_dosen=$_POST['foto_dosen'];
            $nip_dosen=$_POST['nip_dosen'];
            $nama_dosen=$_POST['nama_dosen'];
            $prodi=$_POST['prodi'];
            $fakultas=$_POST['fakultas'];
            
            // validasi
            $sql = "SELECT * FROM dosen WHERE id_dosen='$id_dosen'";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                ?>
                    <div class="alert alert-danger alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong> Data sudah ada</strong>
                    </div>
                <?php
            }else{
            //proses simpan
                $sql = "INSERT INTO dosen(id_dosen,nip_dosen, nama_dosen, prodi, fakultas) VALUES ('$id_dosen','$nip_dosen','$nama_dosen','$prodi','$fakultas')";
                if ($conn->query($sql) === TRUE) {
                    header("Location:dosen.php");
                }
            }
            $conn->close();
        }
?>
<!-- akhir proses tambah data -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Menambahkan data dosen</title>
</head>

<body>
<h1>FORM PENAMBAHAN DATA DOSEN</h1>
    <form action="tambah_dosen.php" method="POST">
        <div class="row justify-content-center">
            <div class="row">
                <div class="col-5 border border-primary mt-2 p-3">
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="nip_dosen">NIP</label>
                            <input type="text" class="form-control" name="nip_dosen" maxlength="25" required>
                        </div>
                    </div>
                    <div>
                        <div class="form-group">
                        <label>Foto Dosen</label>
                            <div class="custom-file">
                                <input required type="file" class="custom-file-input" id="foto" name="foto_dosen">
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="form-group">
                            <label for="nama_dosen">Nama</label>
                            <input type="text" class="form-control" name="nama_dosen" maxlength="255" required>
                        </div>
                    </div>

                    <div class="form-group">
                    <label for="prodi">Prodi</label>
                    <input type="text" class="form-control" name="prodi" maxlength="255" required>
                    </div>
                    <div class="form-group">
                    <label for="fakultas">Fakultas</label>
                    <input type="text" class="form-control" name="fakultas" maxlength="255" required>
                    </div>
                    <input class="btn btn-primary" type="submit" name="simpan" value="Simpan">
                    <a class="btn btn-danger" href="dosen.php">Batal</a>
                </div>
            </div>
        </div>
    </form>
</body>