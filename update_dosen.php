<?php 
include "config.php";

if(isset($_POST['update'])){
    $foto_dosen=$_POST['foto_dosen'];
    $nip_dosen=$_POST['nip_dosen'];
    $nama_dosen=$_POST['nama_dosen'];
    $prodi=$_POST['prodi'];
    $fakultas=$_POST['fakultas'];

    // proses update
    $sql = "UPDATE dosen SET foto_dosen='$foto_dosen',nip_dosen='$nip_dosen',nama_dosen='$nama_dosen',prodi='$prodi',fakultas='$fakultas' WHERE id_dosen='$id_dosen'";
    if ($conn->query($sql) === TRUE) {
        header("Location:dosen.php");
    }
}

// menampilkan data di form
$id_dosen=$_GET['id_dosen'];

$sql = "SELECT * FROM dosen WHERE id_dosen='$id_dosen'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
?>
<!-- akhir proses update data -->


<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Mengubah data dosen</title>
</head>

<body>
<h1>FORM PERUBAHAN DATA DOSEN</h1>
    <form action="update_dosen.php" method="POST">
        <div class="row justify-content-center">
            <div class="row">
                <div class="col-5 border border-primary mt-2 p-3">
                    <div class="mb-3">
                        <div class="form-group">
                        <label for="nip_dosen">NIP</label>
                        <input type="text" class="form-control" value="<?php echo $row['nip_dosen']; ?>" name="nip_dosen" required>
                        </div>
                        <div class="form-group">
                        <label>Foto Dosen</label>
                            <div class="custom-file">
                                <input required type="file" class="custom-file-input" id="foto" name="foto_dosen">
                            </div>
                        </div>
                        <div class="form-group">
                        <label for="nama_dosen">Nama</label>
                        <input type="text" class="form-control" value="<?php echo $row['nama_dosen']; ?>" name="nama_dosen" required>
                        </div>
                        <div class="form-group">
                        <label for="prodi">Prodi</label>
                        <input type="text" class="form-control" value="<?php echo $row['prodi']; ?>" name="prodi" required>
                        </div>
                        <div class="form-group">
                        <label for="fakultas">Fakultas</label>
                        <input type="text" class="form-control" value="<?php echo $row['fakultas']; ?>" name="fakultas" required>
                        </div>
                        <input class="btn btn-primary" type="submit" name="update" value="Update">
                        <a class="btn btn-danger" href="dosen.php">Batal</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    
    <?php
    $conn->close();
    ?>

</body>