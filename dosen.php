<?php
    include "config.php"
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>WEBSITE PENGOLAHAN DATA</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Free Website Template" name="keywords">
        <meta content="Free Website Template" name="description">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">

        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>

    <body>
        <!-- Top Bar Start -->
        <div class="top-bar d-none d-md-block">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="top-bar-left">
                            <div class="text">
                                <h2>SISTEM INFORMASI</h2>
                                <p>PENGOLAHAN DATA DOSEN</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top Bar End -->

        <!-- Nav Bar Start -->
        <div class="navbar navbar-expand-lg bg-dark navbar-dark">
            <div class="container-fluid">
                <a class="navbar-brand">SELAMAT DATANG !</a>
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                    <div class="navbar-nav ml-auto">
                        <a href="index.php" class="nav-item nav-link">Beranda</a>
                        <a href="dosen.php" class="nav-item nav-link active">Dosen</a>
                        <a href="kelas.php" class="nav-item nav-link">Kelas</a>
                        <a href="jadwal.php" class="nav-item nav-link">Jadwal kelas</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Nav Bar End -->

        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>DOSEN</h2>
                        <p>Menambahkan, Merubah atau menghapus data dosen</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->

        <a class="btn btn-primary"href="tambah_dosen.php">Tambah</a>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID DOSEN</th>
                    <th>NIP</th>
                    <th>FOTO</th>
                    <th>NAMA DOSEN</th>
                    <th>PRODI</th>
                    <th>FAKULTAS</th>
                </tr>
            </thead>
            <tbody>
            <!--tempat proses nampilin-->
            <?php
                $sql = "SELECT * FROM dosen ORDER By nip_dosen ASC";
                $result = $conn ->query($sql);
                while($row = $result->fetch_assoc()){
            ?>
            <tr>
                <td><?php echo $row['id_dosen'];?></td>
                <td><?php echo $row['nip_dosen'];?></td>
                <td><?php echo $row['foto_dosen'];?></td>
                <td><?php echo $row['nama_dosen'];?></td>
                <td><?php echo $row['prodi'];?></td>
                <td><?php echo $row['fakultas'];?></td>
                <td>
                    <a class="btn btn-warning" href="update_dosen.php"<?php echo $row['id_dosen'];?>>Edit</a>
                    <a onclick="return confirm ('Apakah yakin data ini akan dihapus ?')" class="btn btn-danger" href="delete_dosen.php=<?php echo $row['id_dosen']; ?>">Delete</a>
                </td>
            </tr>
                </div>
            </form>
            <?php
            }
            $conn->close();
            ?>
            </tbody>
        </table>
    </body>
</html>